package com.example.traderestapi;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.List;

public interface TradeService extends MongoRepository<Trade, Long> {

    List<Trade> findTradesByState(TradeState state);
    long createNewTrade(Trade T);
    Trade findTradeByTradeId(BigInteger id);
    List<Trade> retrieveAllTrades();
     
}
