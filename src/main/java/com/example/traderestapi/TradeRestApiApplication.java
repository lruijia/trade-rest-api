package com.example.traderestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class TradeRestApiApplication {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(TradeRestApiApplication.class, args);
        TradeServiceImpl service = context.getBean(TradeServiceImpl.class);
    }

}
