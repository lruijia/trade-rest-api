package com.example.traderestapi;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "seq_trades")
public class DatabaseSequence {

    @Id
    private String id;

    private long seq;

}
