package com.example.traderestapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

@Component
public class TradeServiceImpl {
    @Autowired
    private TradeService tradeService;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    public Trade retrieveTradeByTradeId(BigInteger id)
    {
        Trade trade = tradeService.findTradeByTradeId(id);
        if (trade != null)
        {
            return trade;
        }
        else
            {
            return null;
        }
    }

    public List<Trade> retrieveAllTrades()
    {
        List<Trade> trade = tradeService.findAll();
        return trade;
    }

    //To be implemented
    public List<Trade> retrieveAllTrades(TradeState tradeState)
    {
        List<Trade> state = tradeService.findTradesByState(tradeState);
        if(state!=null)
        {
            System.out.println("Currentstate" + state);
            return state;
        }
        else
        {
            return null;
        }
    }

    @Transactional
   public long createNewTrade(Trade trade)
   {
       trade.setTradeId(BigInteger.valueOf(sequenceGeneratorService.generateSequence(Trade.SEQUENCE_NAME)));
       tradeService.save(trade);
       return trade.getTradeId().longValue();
   }

}
