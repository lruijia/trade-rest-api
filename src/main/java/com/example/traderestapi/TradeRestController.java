package com.example.traderestapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.net.URI;
import java.util.List;

//Logging exceptions
@Slf4j
@RestController
@RequestMapping("/trade")
@CrossOrigin
public class TradeRestController {

    //autowire service (check name)
  @Autowired
  private TradeService tradeService;


    //Create (POST) Methods
    @PostMapping(consumes = {"application/json", "application/xml"},
            produces = {"application/json", "application/xml"})
    public ResponseEntity createNewTrade(@RequestBody Trade trade){
        try{
            //Change method name according to service. This is assuming returns id.
            long id = tradeService.createNewTrade(trade);

            URI uri = URI.create("/" + id);
            log.info("Created new trade with id: {}", id);
            return ResponseEntity.created(uri).body(trade);
        } catch(Exception e){
            log.error("Unable to create new trade.\n" + e.getMessage());

            //return error with body message
            return ResponseEntity.badRequest().body(e);
        }


        //remove this after uncommenting above
//        return null;
    }


    //Retrieve (GET) Methods
    @GetMapping
    public ResponseEntity<List<Trade>> retrieveAllTradeRecords() {
        List<Trade> tradeList = tradeService.retrieveAllTrades();
        if(tradeList.isEmpty()){
            log.info("No trade records available..");
        } else {
            log.info("All trade records retrieved successfully.");
        }
        return ResponseEntity.ok().body(tradeList);

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Trade> retrieveTradeById(@PathVariable long id) {
        Trade trade = tradeService.findTradeByTradeId(BigInteger.valueOf(id));
        if(trade == null){
            log.error("Trade record with id: {} not found", id);
            return ResponseEntity.notFound().build();
        }
        log.info("Requested trade record: {}", trade.toString());
        return ResponseEntity.ok().body(trade);

    }

    @GetMapping(value = "/")
    public ResponseEntity<List<Trade>> findByState(@RequestParam String state){
        List<Trade> tradesInState = tradeService.findTradesByState(TradeState.valueOf(state));
        if(tradesInState.isEmpty()){
            log.info("No trade records available with state " + state.toUpperCase());
        } else {
            log.info("All trade records with state " + state.toUpperCase() + " retrieved successfully.");
        }
        return ResponseEntity.ok().body(tradesInState);
    }

    /*
     Update and Delete of Trade records should be subject to
     sensible business considerations.

     Assumption:
     1. Trades should be updated by dummy.
     2. Records should not be deleted for audit purposes.
     */
    //Update and "delete" (PUT) Methods


}
